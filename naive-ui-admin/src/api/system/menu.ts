import http from '@/utils/http/axios';
export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}
/**
 * @description: 根据用户id获取用户菜单
 */
export function adminMenus() {
  return http.request({
    url: '/sys-module/menus',
    method: 'GET',
  });
}

/**
 * 获取tree菜单列表
 * @param params
 */
export function getMenuList(params?) {
  return http.request({
    url: '/sys-module/menu/list',
    method: 'GET',
    params,
  });
}
/**
 * @description: 菜单添加/修改
 */
 export function addMenu(params) {
  return http.request<BasicResponseModel>(
    {
      url: '/sys-module/menu',
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}