import http from '@/utils/http/axios';

/**
 * @description: 角色列表
 */
export function getRoleList(params) {
  return http.request({
    url: '/sys-role/role/list',
    method: 'GET',
    params,
  });
}
/**
 * @description: 角色添加/修改
 */
 export function addRole(params) {
  return http.request<BasicResponseModel>(
    {
      url: '/sys-role/role',
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}
/**
 * @description: 角色权限添加/修改
 */
 export function addRoleMenu(params) {
  return http.request<BasicResponseModel>(
    {
      url: '/sys-role/role/menu',
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}
/**
 * @description: 角色删除
 */
 export function delRole(id) {
  return http.request(
    {
      url: `/sys-role/role/${id}`,
      method: 'POST',
    },
    {
      isTransformResponse: false,
    }
  );
}