import { h } from 'vue';
import { NTag } from 'naive-ui';

export const columns = [
  {
    title: 'id',
    key: 'id',
  },
  {
    title: '角色名称',
    key: 'name',
  },
  {
    title: '编号',
    key: 'code',
  },
  {
    title: '备注',
    key: 'remark',
    // render(row) {
    //   return h(
    //     NTag,
    //     {
    //       type: row.isDefault ? 'success' : 'error',
    //     },
    //     {
    //       default: () => (row.isDefault ? '是' : '否'),
    //     }
    //   );
    // },
  },
  {
    title: '创建时间',
    key: 'createTime',
  },
];
