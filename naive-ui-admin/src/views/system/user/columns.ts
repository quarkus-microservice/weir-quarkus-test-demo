import { h } from 'vue';
import { NTag } from 'naive-ui';

export const columns = [
  {
    title: 'id',
    key: 'id',
    width: 50,
  },
  {
    title: '用户名',
    key: 'userName',
    width: 100,
  },
  {
    title: '邮箱',
    key: 'email',
    width: 100,
  },
  {
    title: '备注',
    key: 'remark',
    width: 100,
  },
  {
    title: '状态',
    key: 'enable',
    width: 50,
    render(row) {
      return h(
        NTag,
        {
          type: row.enable ? 'success' : 'error',
        },
        {
          default: () => (row.enable ? '启用' : '禁用'),
        }
      );
    },
  },
  {
    title: '创建时间',
    key: 'createTime',
    width: 100,
  },
];
