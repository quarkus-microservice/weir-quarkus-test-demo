package com.example;

import java.util.List;

public class PageListDTO<T> {

	public List<T> list;
	public Long count;
	public PageListDTO() {
	}
	public PageListDTO(Long count, List<T> list) {
		this.list = list;
		this.count = count;
	}
}
