package com.example;

import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import java.util.Date;


/**
 * 评论
 * @author Administrator
 *
 */
@Entity
public class Comments extends PanacheEntityBase {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer id;
	/**
	 * 文章ID
	 */
	public Integer postsId;
	public Integer userId;
	public String name;
	public String email;
	public String IP;
	public Date createDate = new Date();
	public String content;
	public String approved="1";
	public String profileImageUrl;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
}
