package com.example.config;

import io.quarkus.runtime.StartupEvent;
import io.vertx.ext.consul.ServiceOptions;
import io.vertx.mutiny.ext.consul.ConsulClient;
import io.vertx.ext.consul.ConsulClientOptions;
import io.vertx.mutiny.core.Vertx;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;

@ApplicationScoped
public class Registration {

    @ConfigProperty(name = "consul.host") String consulhost;
    @ConfigProperty(name = "consul.port") int consulport;

    @ConfigProperty(name = "quarkus.http.port", defaultValue = "10804") int app_port;

    /**
     * Register our two services in Consul.
     *
     * Note: this method is called on a worker thread, and so it is allowed to block.
     */
    public void init(@Observes StartupEvent ev, Vertx vertx) {
        ConsulClient client = ConsulClient.create(vertx, new ConsulClientOptions().setHost(consulhost).setPort(consulport));

        client.registerServiceAndAwait(
                new ServiceOptions().setPort(app_port).setAddress("localhost").setName("demo-quarkus-reactive-client").setId("demo-quarkus-reactive-client"));
    }
}
