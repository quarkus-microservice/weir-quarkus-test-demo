package com.example;

import java.util.List;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

import org.hibernate.reactive.mutiny.Mutiny;

import io.smallrye.mutiny.Uni;

@ApplicationScoped
public class PageService {

	@Inject Mutiny.Session mutinySession;
	
	public Uni<Object> pageCount() {
		return mutinySession.createQuery("select count(id) from Comments").getSingleResult();
	}
	
	public Uni<List<Comments>> pageList(Integer page, Integer pageSize) {
		Uni<List<Comments>> resultList = mutinySession.createQuery("select c from Comments c", Comments.class)
				.setFirstResult(page-1)
				.setMaxResults(pageSize)
				.getResultList();
		return resultList;
	}
}
