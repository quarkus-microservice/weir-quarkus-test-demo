package com.example.rest.api;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import com.example.Comments;

import io.smallrye.mutiny.Uni;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@RegisterRestClient(baseUri = "stork://demo-quarkus-reactive-client/client")
public interface Client1Service {
	@GET
    @Path("/{id}")
    public Uni<Comments> get(@PathParam("id") Integer id);
    
    @GET
    @Path("get2/{id}")
    public Uni<String> get2(@PathParam("id") Integer id);
}