package com.example.rest.api;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import com.example.Comments;

import io.smallrye.mutiny.Uni;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@RegisterRestClient(baseUri = "stork://demo-quarkus-reactive-client2/client2")
public interface Client2Service {
	@GET
    @Path("/{id}")
    public Uni<Comments> get(@PathParam("id") Integer id);
    
    @GET
    @Path("get2/{id}")
    public Uni<String> get2(@PathParam("id") Integer id);
}