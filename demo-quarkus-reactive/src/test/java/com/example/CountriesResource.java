package com.example;

import io.smallrye.mutiny.Uni;

import org.eclipse.microprofile.rest.client.inject.RestClient;

import com.example.rest.api.Client1Service;
import com.example.rest.api.Client2Service;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("/weir")
public class CountriesResource {

	@Inject
    @RestClient
    Client1Service client1Service;
	
	@Inject
    @RestClient
    Client2Service client2Service;

	//错误
    @GET
    @Path("get2/{id}")
    public Uni<Object> get2(@PathParam("id") Integer id) {
    	Uni<String> client = client1Service.get2(id);
    	Uni<String> client2 = client2Service.get2(id+1);
    	return Uni.combine().all().unis(client,client2)
    			.combinedWith((c1,c2)->{
    				return c1+c2;
    			});
    }
    //  成功
    @GET
    @Path("/get-client/{id}")
    public Uni<List<Comments>> getClient(@PathParam("id") Integer id) {
    	Uni<Comments> client = client1Service.get(id);
    	Uni<Comments> client2 = client2Service.get(id+1);
    	Uni<List<Comments>> combinedWith = Uni.combine().all().unis(client,client2)
    			.combinedWith(this::computeCommentsList);
    	return combinedWith;
    }
    
    private List<Comments> computeCommentsList(Comments c1, Comments c2) {
    	//合并也去处理
		return Arrays.asList(c1,c2);
	}
    
    
    @GET
    @Path("/{id}")
    public Uni<Comments> get(@PathParam("id") Integer id) {
        return Comments.findById(id);
    }

}