package com.example;

import io.smallrye.mutiny.Uni;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;

@Path("/client2")
public class CountriesResource {

    @GET
    @Path("/{id}")
    public Uni<Comments> get(@PathParam("id") Integer id) {
        return Comments.findById(id);
    }
    @GET
    @Path("get2/{id}")
    public Uni<Comments> get2(@PathParam("id") Integer id) {
    	
    	return Comments.findById(id);
    }
}